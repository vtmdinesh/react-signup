import React from 'react'
import { Link } from 'react-router-dom'
import "./noProduct.css"


function NoProduct() {
    return (
        <div className='no-product-container '>
            <Link to="/" ><button className='btn btn-primary' > Back to Home</button></Link>
        </div>
    )
}

export default NoProduct