import React from 'react'
import { Link } from "react-router-dom"
import "./header.css"


const Header = () => {


    return (
        <div className="header-container m-0 p-0 vh-10 ">
            <nav className="navbar navbar-expand-md navbar-light  vw-100 p-0">
                <Link className="navbar-brand" to="/"><img className="logo" src="./logo.jpeg" alt="logo" /></Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul className="navbar-nav ">
                        <li className="nav-item active">
                            <Link className="link" to="/" > Home </Link>
                        </li>
                        <li className="nav-item">
                            <Link className=" link" to="/products" >Products</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="link" to="/about" >About</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="link" to="/contactUs">Contact Us </Link></li>
                    </ul>
                </div>
            </nav>


        </div>
    )
}

export default Header