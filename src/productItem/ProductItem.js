import React from 'react'
import { Link } from "react-router-dom"
import "./ProductItem.css"



function ProductItem(props) {
    let { title, image, description, price, category, rating, id } = props.productData

    return (
        <div className="col-12 col-sm-5 col-md-3 col-xl-2 shadow m-1 d-flex flex-column justify-content-center align-items-start">
            <img src={image} className="card-img-top img-pi .img-product" alt="product" />
            <div className="card-body pb-1">
                <h5 className="card-title">{title}</h5>
                <h5 className="card-category">{category}</h5>
                <p className="card-text"> price :${price}</p>
                <div className="ratings-container">
                    <p className="card-text"> Rating : {rating.rate}</p>
                    <p className="card-text" >Count: {rating.count}</p></div>
                <Link to={`/products/${id}`} className="button-pi btn btn-primary ml-5 align-self-center">View Product</Link>
            </div>
        </div>

    )
}

export default ProductItem

