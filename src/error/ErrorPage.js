import React from "react"
import { Link } from "react-router-dom"
import "./errorPage.css"
const ErrorPage = (props) => {

    let error = props.error


    console.log(error)

    return (
        <div className="error-container ">
            <div className="text-er text-center">
                <h3>OOPS !!!  </h3>
                <h5> Page Not Found</h5>
                <p>This page was removed or never existed</p>
                <Link to="/" ><button className="btn btn-primary">Back to Home</button></Link>
            </div>

            <img src="./errorPage.jpeg" className="error-img " alt="error" />

        </div>

    )
}

export default ErrorPage