import React, { Component } from "react"
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom"
import './App.css';
import Header from "./header/Header"
import LandingPage from "./landingPage/LandingPage";
import SignupForm from './SignupForm/SignupForm';
import WelcomePage from "./welcomePage/WelcomePage";
import Products from "./products/Products";
import validator from "validator";
import ProductDetails from "./productdetails/ProductDetails";


class App extends Component {
  constructor() {
    super()
    this.state = {
      firstName: "",
      lastName: "",
      firstNameValid: null,
      lastNameValid: null,
      email: "",
      gender: "",
      role: "",
      age: "",
      password: "",
      repeatPassword: "",
      isChecked: "",
      successMsg: "",
      isPwdVisible: false,
      isRepeatPwdVisible: false,
      firstNameErrMsg: "",
      lastNameErrMsg: "",
      emailErrMsg: "",
      ageErrMsg: "",
      genderErrMsg: "",
      roleErrMsg: "",
      pwdErrMsg: "",
      repeatPwdErrMsg: "",
      checkboxErrMsg: "",
      isFormSubmitted: false
    }

  }

  checkFirstName = (firstName) => {
    let isValid = validator.isAlpha(firstName)
    if (isValid && ((firstName).length >= 3)) {
      this.setState({
        firstNameValid: true,
        firstNameErrMsg: "",
        successMsg: ""
      })
      return true
    }
    else {
      this.setState({
        firstNameValid: false,
        firstNameErrMsg: "Please enter Valid first name (First name should contains only alphaphets and  minimum three alphabets required)",
        successMsg: ""
      })
      return false
    }
  }
  checkLastName = (lastName) => {
    let isValid = validator.isAlpha(lastName.trim())
    if (isValid) {
      this.setState({
        lastNameErrMsg: "",
        lastNameValid: true,
        successMsg: "",
      })
      return true
    }
    else {
      this.setState({
        lastNameErrMsg: "Please enter Valid last name (Name should contains only alphabets )",
        lastNameValid: false,
        successMsg: ""
      })
      return false
    }
  }

  emailValidator = (email) => {
    let isValid = validator.isEmail(email)
    console.log("in validator")
    if (isValid) {
      this.setState({
        email: email,
        emailErrMsg: "",
        successMsg: ""

      })
      return true
    }
    else {
      this.setState({
        email: email,
        emailErrMsg: "Please enter valid email",
        successMsg: ""
      })
      return false
    }
  }

  ageValidator = (age) => {
    let isValid = validator.isNumeric(age)

    if (isValid && age >= 13 && age <= 100) {
      this.setState({
        age: age,
        ageErrMsg: "",
        successMsg: ""
      })
      return true
    }
    else if ((isValid && age < 13)) {

      this.setState({
        age: age,
        ageErrMsg: "Sorry !! You should be above 13 to create email.",
        successMsg: ""
      })
      return false
    }
    else if ((isValid && age >= 110)) {
      this.setState({
        age: age,
        ageErrMsg: "Please enter your correct age !!",
        successMsg: ""
      })
      return false
    }
    else {
      this.setState({
        age: age,
        ageErrMsg: "Please enter valid age (age cannot be below 13 and above 110 and it should be a number).",
        successMsg: ""
      })
      return false
    }
  }

  genderValidator = (gender) => {
    if (gender) {
      this.setState({
        gender: gender,
        genderErrMsg: "",
        successMsg: ""
      })
      return true
    }
    else {

      this.setState({
        gender: gender,
        genderErrMsg: "Please select your gender !!",
        successMsg: ""
      })
      return false
    }
  }

  roleValidator = (role) => {
    if (role) {
      this.setState({
        role: role,
        roleErrMsg: "",
        successMsg: ""
      })
      return true
    }
    else {
      this.setState({
        role: role,
        roleErrMsg: "Please select your role !!",
        successMsg: ""
      })
      return false
    }
  }

  passwordValidator = (password) => {
    let isPasswordValid = validator.isStrongPassword(password)
    if (isPasswordValid) {
      this.setState({
        password: password,
        pwdErrMsg: "",
        successMsg: ""
      })
      return true
    }
    else {
      this.setState({
        password: password,
        pwdErrMsg: "Please Enter Valid Password, Password should  contain min one uppercase, lowercase letter , number and a special character with minimum 8 characters long.",
        successMsg: ""
      })
      return false
    }
  }

  passwordChecker = (password, repeatPassword) => {
    let isPasswordValid = validator.isStrongPassword(repeatPassword)
    if (isPasswordValid) {
      let isPasswordsMatch = validator.equals(password, repeatPassword)
      if (isPasswordsMatch) {
        this.setState({
          repeatPassword,
          repeatPwdErrMsg: "",
          successMsg: ""
        })
        return true
      }
      else {
        this.setState({
          repeatPassWord: repeatPassword,
          successMsg: "",
          repeatPwdErrMsg: "Passwords you have entered not match !!",
        })
        return false
      }
    }
    else {
      this.setState({
        repeatPassword,
        successMsg: "",
        repeatPwdErrMsg: "Please Enter Valid Password, Password should  contain min one uppercase, lowercase letter , number and a special character with minimum 8 characters long."
      })
      return false
    }
  }
  checkBoxValidator = (isChecked) => {
    if (isChecked) {
      this.setState({
        isChecked,
        checkboxErrMsg: "",
        successMsg: ""
      })
      return true
    }
    else {
      this.setState({
        isChecked,
        successMsg: "",
        checkboxErrMsg: "Please agree to terms and conditions !!"
      })
      return false
    }
  }

  nameValidation = (event) => {
    let { name, value } = event.target
    // console.log(value)
    this.setState({
      [name]: value.trim()
    })
    if (name === "firstName") {
      this.checkFirstName(value)
    }
    else {
      this.checkLastName(value)
    }

  }
  emailValidation = (event) => {
    let email = event.target.value
    console.log("in validation")
    this.emailValidator(email)
  }
  ageValidation = (event) => {
    let age = event.target.value
    this.ageValidator(age)
  }

  genderValidation = (event) => {
    let gender = event.target.value
    this.genderValidator(gender)
  }

  roleValidation = (event) => {
    let role = event.target.value
    this.roleValidator(role)
  }

  passwordValidation = (event) => {
    let password = event.target.value
    this.passwordValidator(password)
  }

  isPwdMatch = (event) => {
    let password = this.state.password
    // console.log(password)
    let repeatPassword = event.target.value
    this.passwordChecker(password, repeatPassword)
  }

  checkboxValidation = (event) => {
    let isChecked = event.target.checked
    this.checkBoxValidator(isChecked)
  }

  showPassword = () => {
    // console.log(this.state.isPwdVisible)

    this.setState(prevState => ({
      isPwdVisible: !prevState.isPwdVisible
    }))
  }
  showRepeatPassword = () => {
    // console.log(this.state.isPwdVisible)

    this.setState(prevState => ({
      isRepeatPwdVisible: !prevState.isRepeatPwdVisible
    }))
  }

  resetForm = () => {
    console.log("reset clicked")
    this.setState({
      firstName: "",
      lastName: "",
      firstNameValid: null,
      lastNameValid: null,
      email: "",
      gender: "",
      role: "",
      age: "",
      password: "",
      repeatPassword: "",
      isChecked: "",
      successMsg: "",
      isPwdVisible: false,
      isRepeatPwdVisible: false,
      firstNameErrMsg: "",
      lastNameErrMsg: "",
      emailErrMsg: "",
      ageErrMsg: "",
      genderErrMsg: "",
      roleErrMsg: "",
      pwdErrMsg: "",
      repeatPwdErrMsg: "",
      checkboxErrMsg: "",
      isFormSubmitted: false
    })
  }

  validateForm = (event) => {
    event.preventDefault()
    let {
      firstName, lastName, email, age, gender, role, isChecked, password, repeatPassword
    } = this.state
    // console.log(password)
    console.log(this.state.isFormSubmitted)
    let isFirstNameValid = this.checkFirstName(firstName)
    let islastNameValid = this.checkLastName(lastName)
    let isEmailValid = this.emailValidator(email)
    let isAgeValid = this.ageValidator(age)
    let isGenderValid = this.genderValidator(gender)
    let isRoleValid = this.roleValidator(role)
    let isPwdValid = this.passwordValidator(password)
    let isPwdMatch = this.passwordChecker(password, repeatPassword)
    let isAgreed = this.checkBoxValidator(isChecked)

    if (isFirstNameValid && islastNameValid && isEmailValid && isAgeValid && isGenderValid && isRoleValid && isPwdValid && isPwdMatch && isAgreed) {
      console.log({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        age: this.state.age,
        gender: this.state.gender,
        role: this.state.role,
        password: "********",
        repeatPassword: "********",
        isChecked: this.state.isChecked

      })
      this.setState({
        isFormSubmitted: true
      })
    }
  }



  render() {

    return (
      <BrowserRouter >
        <div className="container-app">
          <Header />
          <Switch>
            <Route exact path="/" component={LandingPage} />
            <Route path="/signup" render={(props) => <SignupForm functions={[this.nameValidation, this.emailValidation, this.ageValidation, this.roleValidation, this.genderValidation, this.passwordValidation, this.isPwdMatch, this.showPassword, this.showRepeatPassword, this.checkboxValidation, this.validateForm]} state={this.state} />} />
            <Route
              path="/welcome"
              render={this.state.isFormSubmitted ? ((props) => <WelcomePage state={this.state} reset={this.resetForm} />
              ) : (() => <Redirect to="/signup" />
              )
              } />
            <Route exact path="/products" render={(props) => <Products {...props} />} />
            <Route path={`/products/:id`} component={ProductDetails} />

          </Switch>


        </div>
      </BrowserRouter>
    );
  }

}
export default App
