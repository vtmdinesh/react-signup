import React from 'react'
import "./landingPage.css"
import { Link } from "react-router-dom"
import "./landingPage.css"


const LandingPage = () => {
    return (
        <div className="home-container-lp">
            <div className="text-center text-container-lp">
                <h3 class="text-LP"> The greatest journey of online shop.</h3>
                <button className="button m-2"><Link to="/signup" >Signup</Link></button>
                <button className="button m-2"><Link to="/products" >View Products</Link></button>

            </div></div>
    )
}

export default LandingPage