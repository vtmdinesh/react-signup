import React from 'react'
import "./welcomePage.css"

let welcomePage = (props) => {

    let { firstName, lastName, role, email } = props.state
    let resetForm = props.reset

    console.log(resetForm)


    return (
        <div className='home-container-wp'>
            <div className="text-container-wp">
                <h4 className="text-wp-title">Welcome onboard <br /> {firstName}  {lastName} </h4>
                <p>Your account has been created successfully</p>
                <p>Congratulations on the new position {role}, and many good wishes for your first day.</p>
                <p>We will get back to you on {email} </p>
                <button className="reset-button" onClick={resetForm}>Reset form</button>
            </div>
        </div>
    )
}

export default welcomePage
