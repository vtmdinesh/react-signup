import React from 'react'
import { Redirect } from 'react-router-dom'
import "./signupForm.css"


let SignupForm = (props) => {

    console.log("in")
    let { firstNameValid, lastNameValid, successMsg, age, isPwdVisible, isRepeatPwdVisible, firstNameErrMsg, lastNameErrMsg, emailErrMsg, ageErrMsg, genderErrMsg, roleErrMsg, pwdErrMsg, repeatPwdErrMsg, checkboxErrMsg, isFormSubmitted, firstName, lastName, email, password, repeatPassword } = props.state

    let [nameValidation, emailValidation, ageValidation, roleValidation, genderValidation, passwordValidation, isPwdMatch, showPassword, showRepeatPassword, checkboxValidation, validateForm] = props.functions


    let pwdEyeIconClass = isPwdVisible ? "hide" : null
    let pwdEyeSlashIconClass = isPwdVisible ? null : "hide"
    let passwordInputType = isPwdVisible ? "text" : "password"

    let repeatPwdEyeIconClass = isRepeatPwdVisible ? "hide" : null
    let repeatPwdEyeSlashIconClass = isRepeatPwdVisible ? null : "hide"
    let repeatPasswordInputType = isRepeatPwdVisible ? "text" : "password"



    let firstNameIcon = firstNameValid ? <i className="fa-solid icon fa-user-check"></i> : <i className="fa-solid fa-user icon"></i>

    let lastNameIcon = lastNameValid ? <i className="fa-solid icon fa-user-check"></i> : <i className="fa-solid fa-user icon"></i>

    // console.log(props)


    let renderForm =
        <div className="main-container-sf ">
            <div className="form-container w-75 d-flex flex-column justify-content-start align-items-center">
                <div className="intro">

                    <h3 className="title">Create your account</h3>
                    <p className="success" id="successMsg">
                        {successMsg}
                    </p>
                </div>
                <form id="myForm" onSubmit={validateForm} >
                    <div className="element-container">
                        <div className="image-text-container">
                            {firstNameIcon}
                            <input
                                type="text"
                                placeholder="First name"
                                className="text"
                                name="firstName"
                                value={firstName}
                                id="firstName"
                                onChange={nameValidation}
                            />
                        </div>

                        <p className={`error-msg `} id="firstNameErr">{firstNameErrMsg}</p>
                    </div>
                    <div className="element-container">
                        <div className="image-text-container">
                            {lastNameIcon}
                            <input
                                type="text"
                                placeholder="Last name"
                                className="text text-danger"
                                name="lastName"
                                value={lastName}
                                id="lastName"
                                onChange={nameValidation}
                            />
                        </div>

                        <p className="error-msg" id="lastNameErr">{lastNameErrMsg}</p>
                    </div>
                    <div className="element-container">
                        <div className="image-text-container">
                            <i className="fa-solid icon fa-envelope"></i>
                            <input type="text" placeholder="Email" name="email" value={email} onChange={emailValidation} className="text" id="email" /></div>
                        <p className="error-msg" id="emailErr">{emailErrMsg}</p>
                    </div>
                    <div className="element-container">
                        <div className='image-text-container'>                                           <i className="fa-solid icon fa-crown"></i>
                            <input type="number" className="ageInput" onChange={ageValidation
                            } value={age} placeholder='Enter your age' /></div>

                        <p className="error-msg" id="ageErr">{ageErrMsg}</p>
                    </div>
                    <div className="element-container">
                        <div className="image-text-container">
                            <i className="fa-solid icon fa-transgender"></i>
                            <select id="genderInput" onBlur={genderValidation}>
                                <option className="first-option" value="">Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Not Specified">Not Specified</option>
                            </select></div>
                        <p className="error-msg" id="genderErr">{genderErrMsg}</p>
                    </div>
                    <div className="element-container">
                        <div className="image-text-container">
                            <i className="fa-solid icon fa-user-tie"></i>
                            <select id="roleInput" onBlur={roleValidation}>
                                <option className="first-option" value="">Role</option>
                                <option value="Developer"> Developer</option>
                                <option value="Senior Developer">Senior Developer</option>
                                <option value="Lead Engineer">Lead Engineer</option>
                                <option value="CTO">CTO</option>
                            </select>
                        </div>
                        <p className="error-msg" id="roleErr">{roleErrMsg}</p>
                    </div>
                    <div className="element-container">
                        <div className="image-text-container">
                            <i className="fa-solid icon fa-keyboard"></i>
                            <div className="test">
                                <input
                                    type={passwordInputType}
                                    placeholder="Password"
                                    className="text"
                                    value={password}
                                    id="password"
                                    onChange={passwordValidation}
                                />
                                <div className="eyeicon-container" onClick={showPassword} id="eyeIcon">
                                    <div id="eyeSlash" className={pwdEyeSlashIconClass}>
                                        <i className="fa-solid fa-eye-slash "></i>
                                    </div>
                                    <div id="eye" className={pwdEyeIconClass}>
                                        <i className="fa-solid fa-eye"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p className="error-msg" id="pwdErr">{pwdErrMsg}</p>
                    </div>
                    <div className="element-container">
                        <div className="image-text-container">
                            <i className="fa-solid icon fa-keyboard"></i>
                            <div className="test">
                                <input
                                    value={repeatPassword}
                                    type={repeatPasswordInputType}
                                    placeholder="Re-enter password"
                                    id="repeatPassword"
                                    onChange={isPwdMatch}
                                    className="text"
                                />
                                <div className="eyeicon-container right-margin" onClick={showRepeatPassword} id="eyeIcon2">
                                    <div id="eyeSlash2" className={repeatPwdEyeSlashIconClass}>
                                        <i className="fa-solid fa-eye-slash"></i>
                                    </div>
                                    <div id="eye2" className={repeatPwdEyeIconClass}>
                                        <i className="fa-solid fa-eye" ></i>
                                    </div>
                                </div>
                            </div>                        </div>
                        <p className="error-msg" id="repeatPwdErr">{repeatPwdErrMsg}</p>
                    </div>
                    <div className="checkbox-container">
                        <div className="checkbox">
                            <input type="checkbox" id="checkBox" onChange={checkboxValidation} />
                            <label htmlFor="checkBox">I agree to the terms and conditions</label>
                        </div>
                        <p className="error-msg" id="checkBoxErr">{checkboxErrMsg}</p>
                    </div>
                    <div className='button-container'>
                        <button id="submitBtn" type="submit" className="submit-btn">
                            Sign Up
                        </button>
                    </div>
                </form>
            </div>
        </div>



    let renderComponent = isFormSubmitted ? <Redirect to="/welcome" /> : renderForm



    return (renderComponent)

}


export default SignupForm