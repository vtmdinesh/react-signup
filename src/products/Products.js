import React, { Component } from 'react'
import Spinner from '../spinner/Spinner'
import "./products.css"
import ProductItem from '../productItem/ProductItem'
import ErrorPage from '../error/ErrorPage'
import { Redirect } from 'react-router-dom'
import NoProduct from '../NoProduct/NoProduct'

export class Products extends Component {
    constructor(props) {
        super(props)

        this.APP_STATE = {
            COMPLETED: "COMPLETED",
            IN_PROGRESS: "IN_PROGRESS",
            FAILED: "FAILED",
        }

        this.state = {
            error: {},
            data: []
        }
    }

    componentDidMount() {
        this.getData()


    }

    getData = () => {
        let url = "https://fakestoreapi.com/products"
        this.setState({ currentState: this.APP_STATE.IN_PROGRESS, })
        fetch(url).then(response => {
            return response.json()
        }).then((data) => {

            this.setState({
                data: data,
                isLoading: false
            })
            this.setState({ currentState: this.APP_STATE.COMPLETED, })
        }).catch(error => {
            // console.log("i am inside catch");
            console.log(error.code)
            this.setState({ currentState: this.APP_STATE.FAILED })
            this.setState({ error: error })
            // console.log(error.keys())
        })

    }


    render() {

        let { data } = this.state

        let no_of_items = (Object.keys(data)).length
        // console.log(data[0])
        return (
            <div className=' products-container d-flex flex-column justify-content-center align-items-center'>
                <h1 className='products-title'>Products</h1>
                <div className='container'>
                    <div className="row d-flex justify-content-between row-p">

                        {(this.state.currentState === this.APP_STATE.IN_PROGRESS) ? <Spinner /> : null}
                        {(this.state.currentState === this.APP_STATE.FAILED) ? <ErrorPage error={this.state.error} /> : null}

                        {(this.state.currentState === this.APP_STATE.COMPLETED) ? ((no_of_items) ? (data.map(eachData => {
                            return <ProductItem productData={eachData} key={eachData.id}
                            />
                        })) : <NoProduct />


                        ) : null}

                    </div>
                </div>

            </div>
        )
    }
}

export default Products