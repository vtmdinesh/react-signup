import React, { Component } from 'react'
import "./productDetails.css"
import Spinner from '../spinner/Spinner'
import ErrorPage from '../error/ErrorPage'
import NoProduct from '../NoProduct/NoProduct'

export default class ProductDetails extends Component {

    constructor() {
        super()
        this.APP_STATE = {
            COMPLETED: "COMPLETED",
            IN_PROGRESS: "IN_PROGRESS",
            FAILED: "FAILED"
        }
        this.state = { productDetails: [] }

    }
    componentDidMount() {
        this.getProductData()
    }
    getProductData = () => {
        let { match } = this.props
        let { id } = match.params
        this.setState({ currentState: this.APP_STATE.IN_PROGRESS })
        let url = `https://fakestoreapi.com/products/${id}`

        fetch(url).then(res => res.json()).then(data => {
            this.setState({
                productDetails: data,
                currentState: this.APP_STATE.COMPLETED
            })

        }).catch(err => {
            console.error(err)
            this.setState({
                currentState: this.APP_STATE.FAILED
            })
        })

    }



    render() {
        // console.log("in")
        // let productKeys = Object.keys(this.state.productDetails)

        let currentState = this.state.currentState
        console.log(currentState)


        // let { rate, count } = rating


        let detailsPage = <div className="container-pd d-flex  flex-column align-items-center flex-md-row pt-2">
            <div className="image-container-pd">
                <img src={this.state.productDetails.image} alt="product" className="image-pd" />

                <div className="d-flex w-100 flex-column flex-md-row justify-content-center justify-content-md-around mt-3">

                    <button className="add-cart btn m-2">ADD TO CART</button>
                    <button className="buy-now btn m-2">BUY NOW</button>

                </div>
            </div>

            <div className="pl-3 pr-1 details-container-pd ">

                <h3 className="title-pd mt-2">
                    {this.state.productDetails.title}</h3>
                <div className="rating-details-container">
                    <p>{ }</p>
                    <p>{ }</p>
                </div>
                <p className='description-pd'>{this.state.productDetails.description}</p>
                <p className="price"> Price: ${this.state.productDetails.price}</p>
            </div>
        </div>

        return (
            <div className="d-flex flex-column justify-content-center align-items-center main-container-pd">
                {
                    (this.state.currentState === this.APP_STATE.IN_PROGRESS) ? <Spinner /> : null
                }
                {
                    (this.state.currentState === this.APP_STATE.FAILED) ? <ErrorPage /> : null
                }
                {
                    (this.state.currentState === this.APP_STATE.COMPLETED) ? ((Array.isArray(this.state.productDetails)) ? <NoProduct /> : detailsPage) : null
                }


            </div>



        )
    }
}
